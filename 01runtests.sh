#!/bin/bash
# Parameters
SCRIPT_LOCATION=/home/nvidia/git/orb-slam-openvx-exploration
BIN_LOCATION=/home/nvidia/git/orb-slam-openvx/script_exploration
ORB_VOC=/home/nvidia/git/orb-slam-openvx/Vocabulary/ORBvoc.txt
EUROC_BASE_PATH=/home/nvidia/Downloads
EUROC_YAML_FILE=/home/nvidia/git/orb-slam-openvx/Examples/Monocular/EuRoC.yaml

# Id of the runs (for multiple runs, EXPLICIT set values e.g. RUNS=(1 2 3)
RUNS=1
# Various power model configurations. For more info, see nvpmodel.conf
MODELS=(10 13 20 23 30 33)
# The FPS multiplier
FPS_MULTIPLIER=(1 1.5 2)
# Folder names of the sequences
SEQUENCES=(MH_01)

# This are the versions to be run. By default, only version 1 is to be processed
# - Version 0: like the original ORB-SLAM, process every frame
# - Version 1: process only the frames that we are capable in the given time budget, including the img load time
# - Version 2: process only the frames that we are capable in the given time budget, excluding the img load time
VERSIONS=(1)



# Script stuff

if (( $EUID != 0 )); then
    echo "To avoid problem with settings, run this as root user (not sudo)"
    echo "Example:"
    echo "  sudo su"
    echo "  ./01runtests.sh"
    exit
fi

ORB_STANDARD_DIR=$BIN_LOCATION/00standard/
ORB_STANDARD=$BIN_LOCATION/00standard/mono_euroc

ORB_MP_DIR=$BIN_LOCATION/01mp
ORB_MP=$BIN_LOCATION/01mp/mono_euroc

ORB_VX_DIR=$BIN_LOCATION/02vx
ORB_VX=$BIN_LOCATION/02vx/mono_euroc

ORB_VX_MP_DIR=$BIN_LOCATION/03vxmp
ORB_VX_MP=$BIN_LOCATION/03vxmp/mono_euroc

ORB_VX_CUDA_DIR=$BIN_LOCATION/04vxcuda
ORB_VX_CUDA=$BIN_LOCATION/04vxcuda/mono_euroc

ORB_VX_MP_CUDA_DIR=$BIN_LOCATION/05vxmpcuda
ORB_VX_MP_CUDA=$BIN_LOCATION/05vxmpcuda/mono_euroc

mkdir -p $SCRIPT_LOCATION/output
cd $SCRIPT_LOCATION/output

function run_test {
    NUM="$1"
    SUFFIX=$5
    
    echo -n "** Run n. $3) Version to be run: "
    case "$2" in
    0)
        echo -n "Without frame skipping"
        VERSION=""
        ;;
    1)
        echo -n "With frame skipping, read time included"
        VERSION="skip"
        ;;
    2)
        echo -n "With frame skipping, read time excluded"
        VERSION="skip_noimgtime"
        ;;
    *)
        echo -n "Nothing here, so without frame skipping assumed"
        VERSION=""
        ;;
    esac
    IDRUN=$3
    #TARGETFPS=$(LC_NUMERIC=C echo 20*$4 | bc | xargs printf "%.0f")
    TARGETFPS=$4
    
    echo ", $(LC_NUMERIC=C echo 20*$4 | bc | xargs printf %.0f) FPS with model $SUFFIX"
    
    # To enable per-sequence settings, following instruction can be used
    # EUROC_YAML_FILE="$EUROC_BASE_PATH/${NUM}/EuRoC.yaml
    sed -i "s/Camera\.fps.*/Camera.fps: $(LC_NUMERIC=C echo 20*$4 | bc | xargs printf %.0f)/" $EUROC_YAML_FILE

    ARGS="$ORB_VOC $EUROC_YAML_FILE $EUROC_BASE_PATH/${NUM}/cam0/data $EUROC_BASE_PATH/${NUM}/cam0/data.csv $TARGETFPS $VERSION" 
        
    ARM_CPU_ACTIVE=$(cat /sys/devices/system/cpu/cpu3/online)
    if [ "$ARM_CPU_ACTIVE" = "1" ];
    then
        echo "Sequential version ARM ${NUM}"
        export LD_LIBRARY_PATH=$ORB_STANDARD_DIR
        taskset -c 3 $ORB_STANDARD $ARGS &> seqARM${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
        mv KeyFrameTrajectory.txt trajectory_seqARM${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    fi

    DENVER_CPU_ACTIVE=$(cat /sys/devices/system/cpu/cpu1/online)
    if [[ "$DENVER_CPU_ACTIVE" = "1" ]];
    then
        echo "Sequential version Denver ${NUM}"
        export LD_LIBRARY_PATH=$ORB_STANDARD_DIR
        taskset -c 1 $ORB_STANDARD $ARGS &> seqDenver${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
        mv KeyFrameTrajectory.txt trajectory_seqDenver${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    fi

    CLUSTER_CPU_ACTIVE=$(cat /sys/devices/system/cpu/online)
    if [[ "$CLUSTER_CPU_ACTIVE" = "0-2" ]];
    then
        # only the DENVER CPUs!!
        echo "Multithreaded version ${NUM}"
        export LD_LIBRARY_PATH=$ORB_STANDARD_DIR
        taskset -c 1-2 $ORB_STANDARD $ARGS &> multi${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
        mv KeyFrameTrajectory.txt trajectory_multi${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    else
        echo "Multithreaded version ${NUM}"
        export LD_LIBRARY_PATH=$ORB_STANDARD_DIR
        $ORB_STANDARD $ARGS &> multi${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
        mv KeyFrameTrajectory.txt trajectory_multi${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    fi
    
    echo "MP version ${NUM}"
    export LD_LIBRARY_PATH=$ORB_MP_DIR
    $ORB_MP $ARGS &> mp${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    mv KeyFrameTrajectory.txt trajectory_mp${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt

    echo "VX version ${NUM}"
    export LD_LIBRARY_PATH=$ORB_VX_DIR
    $ORB_VX $ARGS &> vx${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    mv KeyFrameTrajectory.txt trajectory_vx${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt

    echo "VX MP version ${NUM}"
    export LD_LIBRARY_PATH=$ORB_VX_MP_DIR
    ${ORB_VX_MP} $ARGS &> vxmp${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    mv KeyFrameTrajectory.txt trajectory_vxmp${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    
    echo "VX CUDA version ${NUM}"
    export LD_LIBRARY_PATH=$ORB_VX_CUDA_DIR
    ${ORB_VX_CUDA} $ARGS &> vxcuda${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    mv KeyFrameTrajectory.txt trajectory_vxcuda${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
    
    echo "VX MP CUDA version ${NUM}"
    export LD_LIBRARY_PATH=$ORB_VX_MP_CUDA_DIR
    ${ORB_VX_MP_CUDA} $ARGS &> vxmpcuda${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    mv KeyFrameTrajectory.txt trajectory_vxmpcuda${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.txt
}

# Try to cool the Jetson
echo "255" > /sys/kernel/debug/tegra_fan/target_pwm

# The run
for RUN in ${RUNS[*]}; do
    # The power model to be checked
    for MODEL in ${MODELS[*]}; do
        sudo nvpmodel -f $SCRIPT_LOCATION/nvpmodel.conf -m 10
        sudo nvpmodel -f $SCRIPT_LOCATION/nvpmodel.conf -m $MODEL
        
        for NUM in ${SEQUENCES[*]}; do
            for FPS in ${FPS_MULTIPLIER[*]}; do
                for v in ${VERSIONS[*]}; do
                    run_test ${NUM} $v $RUN $FPS $MODEL
                done
            done
        done
    done
done
