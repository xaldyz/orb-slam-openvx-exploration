#!/usr/bin/env python3

# Terzo da lanciare

import sqlite3
import csv
import os
import sys

os.chdir('/home/nvidia/Desktop/outputultimo/')

OUTFILE='/home/nvidia/Desktop/outputultimo/output03_datimappingloop.csv'

fd = open(OUTFILE, 'w')
cfile = csv.writer(fd, delimiter=';')
cfile.writerow(['sequence','version','core','frequenze','skipping','fase','tempototale','tempomedio','tempomassimo','varianza'])

def loadandsave(path, versione, ncore, frequenze, skipping, fase, seq):
    data = list()
    fdd = open(path,'r')
    inputf = csv.reader(fdd, delimiter=';')
    for line in inputf:
        data.append(int(line[0]))
    fdd.close()
    
    somma = 0
    media = 0
    massimo = -1
    varianza = 0
    conteggio = len(data)
    minsqrt = 0
    
    for d in data:
        somma = somma + d
    media = somma/conteggio
    for d in data:
        minsqrt = minsqrt + (d-media)*(d-media)
        if d > massimo:
            massimo = d
    varianza = minsqrt/conteggio

    cfile.writerow([seq,versione,ncore,frequenze,skipping,fase,somma,media,massimo,varianza])


# -------------------------------------------------------------------------------------------------
for NUM in ['11','13']:
    for VERSIONE in ['seq','multi','mp','vx','vxmp','vxcuda','vxmpcuda']:
        for CORE in ['4','6']:
            for FREQUENZE in ['dinamiche','massime']:
                for SKIPPING in ['si','no']:
                    for FASE in ['Tracking','Mapping','LoopClosing']:
                        name = 'timesTotal%s%s%s%s%s%s.csv' % (
                            FASE,
                            VERSIONE,
                            'max' if FREQUENZE == 'massime' else '',
                            '6' if CORE == '6' else '',
                            'skip' if SKIPPING == 'si' else '',
                            NUM
                        )
                        loadandsave(name, VERSIONE, CORE, FREQUENZE, SKIPPING, FASE, NUM)

fd.close()
print('Written file %s' % OUTFILE)

