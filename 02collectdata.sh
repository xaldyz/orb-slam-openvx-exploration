#!/bin/bash

DATA_LOCATION=/home/nvidia/git/orb-slam-openvx/script_exploration/output

# Secondo da lanciare

cd $DATA_LOCATION

# Id of the runs (for multiple runs, EXPLICIT set values e.g. RUNS=(1 2 3)
RUNS=(1)
# Various power model configurations. For more info, see nvpmodel.conf
MODELS=(10 13 20 23 30 33)
# The FPS multiplier
FPS_MULTIPLIER=(1 1.5 2)
# Folder names of the sequences
SEQUENCES=(MH_01)






# Script stuff

if (( $EUID != 0 )); then
    echo "To avoid problem with settings, run this as root user (not sudo)"
    echo "Example:"
    echo "  sudo su"
    echo "  ./02collectdata.sh"
    exit
fi

echo "run;modello;sequenza;FPS;versione;core;frequenze cpu ARM;frequenze cpu Denver;frequenze gpu;frequenze memoria;skipping;tempototale;maxpower;energy;trueenergy;nsamples;nimages;ttotaltracking (s);tsingleorb normale (ms);tsingleorb init (ms);frame tracked;frame processed;num reinit;num frames double" > results.csv

function echo_shot {
    PREFIX=$1
    NUM=$2
    case "$3" in
    0)
        SKIP=""
        FSKIP="no skip"
        ;;
    1)
        SKIP="skip"
        FSKIP="skip"
        ;;
    2)
        SKIP="skip_noimgtime"
        FSKIP="skip no time"
        ;;
    *)
        SKIP=""
        FSKIP="no skip"
        ;;
    esac
    MODEL="$4"
    FREQUENCIES="dinamiche"
    IDRUN="$5"
    TARGETFPS="$6"
    #TARGETFPS=$(LC_NUMERIC=C echo 10*$6 | bc | xargs printf "%.0f")
    
#    seqDenver${VERSION}_${SUFFIX}_${NUM}_${IDRUN}_${TARGETFPS}.log
    FNAME=$PREFIX${SKIP}_${MODEL}_${NUM}_${IDRUN}_${TARGETFPS}.log
#    echo $FNAME
    if [ ! -e $FNAME ]
    then
        return;
    fi
    

    TIME=$(cat $FNAME | grep "TOTALTIME:" | cut -d: -f2)
    MAXPO=$(cat $FNAME | grep "MAXPOWER:" | cut -d: -f2)
    ENERGY=$(cat $FNAME | grep "^ENERGY:" | cut -d: -f2)
    TRUE_ENERGY=$(cat $FNAME | grep "TRUE_ENERGY:" | cut -d: -f2)
    SAMPLES=$(cat $FNAME | grep "NUMSAMPLES:" | cut -d: -f2)
    IMAGES=$(cat $FNAME | grep "NUMIMAGES:" | head -n 1 | cut -d: -f2)
    TIMETRACKING=$(cat $FNAME | grep "TIMETRACKING:" | cut -d: -f2)
    TIMELC=$(cat $FNAME | grep "TIMELC:" | cut -d: -f2)
    TIMEMAPPING=$(cat $FNAME | grep "TIMEMAPPING:" | cut -d: -f2)
    TIMESINGLE_1=$(cat $FNAME | grep "Avg computed frame ORB" | cut -d: -f2 | cut -dm -f1 | head -n 1)
    TIMESINGLE_INIT=$(cat $FNAME | grep "Avg computed frame ORB" | cut -d: -f2 | cut -dm -f1 | tail -n 1)
    ACCURACY=$(cat $FNAME | grep "Tracked frames" | cut -d: -f2  | cut -d"/" -f1)
    IMG_PROC=$(cat $FNAME | grep "Tracked frames" | cut -d: -f2  | cut -d"/" -f2 | cut -d" " -f1)
    REINIT=$(cat $FNAME | grep "REINIT" | cut -d: -f2)
    DOUBLE_COUNT=$(cat $FNAME | grep "DOUBLE_" | cut -d: -f2)
    
    echo "$IDRUN;$MODEL;$NUM;$TARGETFPS;$PREFIX;$CPUS;$FREQUENCIES_CPU_ARM;$FREQUENCIES_CPU_DENVER;$FREQUENCIES_GPU;$FREQUENCIES_MEMORY;$FSKIP;$TIME;$MAXPO;$ENERGY;$TRUE_ENERGY;$SAMPLES;$IMAGES;$TIMETRACKING;$TIMESINGLE_1;$TIMESINGLE_INIT;$ACCURACY;$IMG_PROC;$REINIT;$DOUBLE_COUNT" >> results.csv
}

for RUN in ${RUNS[*]}; do
for NUM_SEQ in ${SEQUENCES[*]}; do
    for SEQ_STR in "seqARM" "seqDenver" "multi" "mp" "vx" "vxmp" "vxcuda" "vxmpcuda" ; do
        for MODE_SKIP in ${VERSIONS[*]}; do
        for MODEL_ARG in ${MODELS[*]}}; do
            for FPS in ${FPS_MULTIPLIER[*]}; do
                echo_shot $SEQ_STR $NUM_SEQ $MODE_SKIP $MODEL_ARG $RUN $FPS
            done
        done
        done
    done
done
done
